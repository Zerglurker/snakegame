// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME20_SnakeGame20GameModeBase_generated_h
#error "SnakeGame20GameModeBase.generated.h already included, missing '#pragma once' in SnakeGame20GameModeBase.h"
#endif
#define SNAKEGAME20_SnakeGame20GameModeBase_generated_h

#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_RPC_WRAPPERS
#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGame20GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SnakeGame20"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame20GameModeBase)


#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGame20GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SnakeGame20"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame20GameModeBase)


#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame20GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame20GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame20GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame20GameModeBase(ASnakeGame20GameModeBase&&); \
	NO_API ASnakeGame20GameModeBase(const ASnakeGame20GameModeBase&); \
public:


#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame20GameModeBase(ASnakeGame20GameModeBase&&); \
	NO_API ASnakeGame20GameModeBase(const ASnakeGame20GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame20GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame20GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame20GameModeBase)


#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_12_PROLOG
#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_RPC_WRAPPERS \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_INCLASS \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME20_API UClass* StaticClass<class ASnakeGame20GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame20_Source_SnakeGame20_SnakeGame20GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
