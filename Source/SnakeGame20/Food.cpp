// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeGame20GameModeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//auto Gamemode = GetGameInstance<ASnakeGame20GameModeBase>();
	//Gamemode->Foods++;
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead) {
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake)){
			Snake->AddSnakeElement();
		}
	}

	//auto Gamemode = GetGameInstance<ASnakeGame20GameModeBase>();
	//Gamemode->Foods--;

	Destroy();
}

