// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "SnakeBase.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandleVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandleHorizontalInput);
	
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeBase = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	//��
}

void APlayerPawnBase::HandleVerticalInput(float value)
{

	if (IsValid(SnakeBase)) {
		if (value > 0 && SnakeBase->LastMoveDirection != EMovementDirection::DOWN) {
			SnakeBase->LastMoveDirection = EMovementDirection::UP; 
		}
		else if (value < 0 && SnakeBase->LastMoveDirection != EMovementDirection::UP) {
			SnakeBase->LastMoveDirection = EMovementDirection::DOWN; 
		}
	}
	
}

void APlayerPawnBase::HandleHorizontalInput(float value)
{
	if (IsValid(SnakeBase)) {
		if (value > 0 && SnakeBase->LastMoveDirection != EMovementDirection::LEFT) {
			SnakeBase->LastMoveDirection = EMovementDirection::RIGHT;
		}else if(value<0 && SnakeBase->LastMoveDirection != EMovementDirection::RIGHT){
			SnakeBase->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
	
}

