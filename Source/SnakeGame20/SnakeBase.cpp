// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 100;
	LastMoveDirection = EMovementDirection::LEFT;
}

ASnakeBase::~ASnakeBase()
{
	for (int i = SnakeElements.Num() - 1;i >= 0;i--) {
		auto Element = SnakeElements[i];
		if (Element) {
			//GetWorld()->DestroyActor(Element);
			//SnakeElements[i]-> MeshComponent;
			SnakeElements[i]-> MeshComponent = nullptr;
		}
	}
	//SnakeElements.Empty();
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4, true);

	MovementSpeed = ElementSize;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum, bool IsFirst)
{
	//GetActorTransform()
	for (int i = 0; i < ElementsNum;i++) {

		FVector NewLocation;
		if (IsFirst) {
			switch (LastMoveDirection)
			{
				//������� � �������� ����������� � �������� ������
			case EMovementDirection::UP:
				NewLocation = FVector(-SnakeElements.Num() * ElementSize, 0, 0);
				break;
			case EMovementDirection::DOWN:
				NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
				break;
			case EMovementDirection::LEFT:
				NewLocation = FVector(0, -SnakeElements.Num() * ElementSize, 0);
				break;
			case EMovementDirection::RIGHT:
				NewLocation = FVector(0, SnakeElements.Num() * ElementSize, 0);
				break;
			default:
				break;
			}
		}
		else {
			NewLocation = SnakeElements.Last()->GetActorLocation();
		}
		FTransform Transform = FTransform(NewLocation);
		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, Transform); //��������� ����� ���� �� ���������� ���������������� � BP ������ ����������!
		//NewElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);                     //�������� ������� � ������
		NewElement->SnakeOwner = this;
		int32 index = SnakeElements.Add(NewElement);                                                            //������� � ������
		if (index == 0) {
			NewElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	//float MovementDelta = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeed;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	//� ����������� ���������� ������ ������, ��� ����� ������ ��������������� �� ����� ����������� ��������
	//AddActorWorldOffset(MovementVector);
	for (int i = SnakeElements.Num() - 1;i > 0;i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		if (IsValid(CurrentElement) && IsValid(PrevElement)) {

			FVector PrevLocation = PrevElement->GetActorLocation();
			CurrentElement->SetActorLocation(PrevLocation);
		}
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 Index;
		SnakeElements.Find(OverlappedElement, Index);
		bool bIsFirst = Index == 0;

		IInteractable* InteractableObject = Cast<IInteractable>(Other);
		if (InteractableObject) {
			InteractableObject->Interact(this, bIsFirst);

		}
	}
}

